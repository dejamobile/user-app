import React from 'react';
import './App.css';
import './mini-default.min.css';
import BankUI from './component/BankUI';

function App() {
  return (
    <div><BankUI/></div>
  );
}

export default App;
