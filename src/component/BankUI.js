import React, { Component } from 'react';
import cogoToast from 'cogo-toast';

class BankUI extends Component {
    constructor(props) {
        super(props);
        this.state = {records : [], token:"", cards : []};
    }

    getCardFromBank() {
        var myHeaders = new Headers();
        myHeaders.append("authorization", "123456")
        fetch("http://localhost:3001/user/info",{mode: 'cors', headers: myHeaders})
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(prevStat => ({...prevStat, cards:[...prevStat.cards, result]}));
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                }
            )
    }

    getCardNumberFromCloud(index, cardNumber) {
        fetch("http://localhost:8080/cards?cardType=visa&cardNumber="+cardNumber,{mode: 'cors'})
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(prevStat => {
                        prevStat.cards[index].token = result.digitalCardNumber;
                        prevStat.cards[index].type = result.cardType;
                        return {...prevStat, cards : prevStat.cards}
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                }
            )
    }

    payTransaction(token) {
        fetch("http://localhost:3002/payment",{mode: 'cors', method:'POST', headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }, body:JSON.stringify({token:token, amount : 15})})
            .then(res => res.json())
            .then(
                (result) => {
                    cogoToast.success("La transaction a bien été effectuée.");
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                }
            )
    }

    getPaymentHistory(token) {
        fetch("http://localhost:3002/payment/token/"+token,{mode: 'cors'})
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(prevStat => {
                        return {...prevStat, records: result, token:token}
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                }
            )
    }

    render() {
        return (
        <div>
        <div>
            <button onClick={()=> this.getCardFromBank()}>Demander un numéro de carte</button>
        <table className="table">
            <thead>
            <tr>
                <th>Propriétaire</th>
                <th>Numéro de carte</th>
                <th></th>
                <th>Type</th>
                <th>Token</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {this.state.cards.map((card, index) => <tr><td>{card.name}</td><td>{card.cardNumber}</td>
            <td><button onClick={()=> this.getCardNumberFromCloud(index, card.cardNumber)}>Demander un token</button></td>
            <td>{card.type}</td>
            <td>{card.token}</td>
            {card.token != undefined && 
            <td><button onClick={()=> {this.payTransaction(card.token)}}>Effectuer une transaction</button></td>}
            {card.token != undefined && 
            <td><button onClick={()=> {this.getPaymentHistory(card.token)}}>Afficher l'historique de mes paiements</button></td>}
            </tr>)}
            </tbody>
        </table>
        </div>
        <div>
<h2>Historique des paiements du token {this.state.token} </h2>
        <table className="table">
            <thead>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Card Number</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            {this.state.records.map((record) => <tr>
                <td>{record.date}</td><td>{record.type}</td><td>{record.cardNumber}</td><td>{record.amount}</td>
            </tr>)}
            </tbody>
        </table>
        </div>
        </div>);
    }
}

export default BankUI;